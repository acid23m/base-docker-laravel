<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Repositories\Home\MetaRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    public function testHomepage(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testDefaultMetaData(): void
    {
        $this->assertDatabaseMissing('home_metas', ['id' => 1]);

        $meta = resolve(MetaRepository::class)->getFullData();

        $this->assertDatabaseHas('home_metas', [
            'id' => 1,
            'title' => $meta['title'],
        ]);

        $response = $this->get('/');

        $response->assertSeeText($meta['title']);
    }

}
