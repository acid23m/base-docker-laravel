<?php /** @var App\Models\Home\Meta $model */ ?>

{{ html()->modelForm($model, $method, $route)->attribute('enctype', 'multipart/form-data')->open() }}


<div class="form-group">
    {{ html()->label($model::getAttributeLabel('title'), 'title') }}

    {{ html()->text('title')->class([
        'form-control',
        'is-invalid' => $errors->has('title'),
    ]) }}

    @error('title')
    {{ html()->span($message)->class('invalid-feedback') }}
    @enderror
</div>


<div class="form-group">
    {{ html()->label($model::getAttributeLabel('description'), 'description') }}

    {{ html()->textarea('description')->class([
        'form-control',
        'is-invalid' => $errors->has('description'),
    ]) }}

    @error('description')
    {{ html()->span($message)->class('invalid-feedback') }}
    @enderror
</div>


@if($model->image)
    <img class="img-thumbnail" src="{{ $image_url ?? '' }}" alt="">
@endif

<div class="custom-file mt-3 mb-4">
    {{ html()->file('image')->class([
        'custom-file-input',
        'is-invalid' => $errors->has('image'),
    ]) }}

    {{ html()->label($model::getAttributeLabel('image'), 'image')
        ->class('custom-file-label')
        ->data('browse', trans('Choose file')) }}

    @error('image')
    {{ html()->span($message)->class('invalid-feedback') }}
    @enderror
</div>

@if($model->image)
    <div class="custom-control custom-checkbox">
        {{ html()->checkbox('del_image')->class([
            'custom-control-input',
            'is-invalid' => $errors->has('del_image')
        ]) }}

        {{ html()->label(__('Delete file'), 'del_image')->class('custom-control-label') }}

        @error('del_image')
        {{ html()->span($message)->class('invalid-feedback') }}
        @enderror
    </div>
@endif


{{ html()->submit($submit['label'])->class('mt-3 btn btn-' . $submit['type']) }}


{{ html()->closeModelForm() }}
