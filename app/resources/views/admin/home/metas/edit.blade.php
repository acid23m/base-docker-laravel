@extends('admin::layouts.pages')

@push('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('admin.home.metas.show') }}">{{ __('Meta data of the Homepage') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('Edit') }}</li>
@endpush

@section('title', __('Meta data of the Homepage') . ': ' . __('Edit'))

@section('content')

    @include('admin.home.metas._form', [
        'method' => 'put',
        'route' => route('admin.home.metas.update'),
        'submit' => [
            'type' => 'primary',
            'label' => __('Save'),
        ]
    ])

@endsection
