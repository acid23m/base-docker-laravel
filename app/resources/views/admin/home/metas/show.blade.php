@extends('admin::layouts.pages')

@push('breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">{{ __('Meta data of the Homepage') }}</li>
@endpush

@section('title', __('Meta data of the Homepage'))

@section('content')

    <div class="d-flex mb-3">
        <a class="btn btn-primary mr-2" href="{{ route('admin.home.metas.edit') }}">{{ __('Edit') }}</a>
    </div>

    @modelDetails($modeldetails_config)

@endsection
