@php $current_route_name = request()->route()->getName() @endphp

<div class="sidebar sidebar-dark bg-dark pt-3">
    <ul class="list-unstyled">
        <li class="{{ $current_route_name === 'admin.home' ? 'active' : '' }}">
            <a href="{{ route('admin.home') }}">
                <i class="fa fa-fw fa-home mr-1"></i>
                {{ __('Home') }}
            </a>
        </li>
    </ul>

    {{--<ul class="list-unstyled mt-3">
        <li>
            <a href="#">
                <i class="fab fa-fw fa-affiliatetheme mr-1"></i>
                Menu Item
            </a>
        </li>
        <li>
            <a data-toggle="collapse" href="#sm_expand_1">
                <i class="fas fa-fw fa-archive mr-1"></i>
                Expandable Menu Item
            </a>
            <ul class="list-unstyled collapse" id="sm_expand_1">
                <li><a href="#">Submenu Item</a></li>
                <li><a href="#">Submenu Item</a></li>
            </ul>
        </li>
        <li><a href="#"><i class="fas fa-fw fa-asterisk mr-1"></i> Menu Item</a></li>
        <li><a href="#"><i class="fas fa-fw fa-baseball-ball mr-1"></i> Menu Item</a></li>
    </ul>--}}

    <ul class="list-unstyled mt-3">
        <li class="{{ Str::startsWith($current_route_name, 'admin.home.') ? 'active' : '' }}">
            <a data-toggle="collapse" href="#sm_expand_1"
               aria-expanded="{{ Str::startsWith($current_route_name, 'admin.home.') ? 'true' : 'false' }}">
                <i class="fa fa-fw fa-desktop mr-1"></i>
                {{ __('Site Homepage') }}
            </a>
            <ul class="list-unstyled collapse {{ Str::startsWith($current_route_name, 'admin.home.') ? 'show' : '' }}"
                id="sm_expand_1">
                <li class="{{ $current_route_name === 'admin.home.metas.show' ? 'active' : '' }}">
                    <a href="{{ route('admin.home.metas.show') }}">
                        <i class="fa fa-fw fa-angle-right mr-1"></i>
                        {{ __('Page meta-data') }}
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
