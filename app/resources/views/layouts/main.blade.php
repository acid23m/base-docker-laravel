<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">
    <meta http-equiv="X-DNS-Prefetch-Control" content="on">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <base href="{{ url('/') }}">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">

    <link rel="preconnect" href="//fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="//fonts.googleapis.com" crossorigin>

    {!! app('seotools')->generate(true) !!}

    @prepend('styles')
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @endprepend
    @stack('styles')

    {!! analytics() !!}
    {!! head_scripts() !!}
</head>
<body>

{{--body--}}
@yield('body')

@prepend('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
@endprepend
@stack('scripts')

{!! bottom_scripts() !!}
</body>
</html>
