<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

final class {{ class }} extends Migration
{
    private string $table_name = '{{ table }}';

    /**
     * Runs the migrations.
     */
    public function up(): void
    {
        Schema::create($this->table_name, static function (Blueprint $table) {
            $table->id();

            $table->string('page_title')->nullable();
            $table->text('page_description')->nullable();
            $table->boolean('active')->default(true);
            $table->string('slug');
            $table->unsignedInteger('position')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->index([
                'active',
                'slug',
                'position',
                'deleted_at',
            ]);
        });
    }

    /**
     * Reverses the migrations.
     */
    public function down(): void
    {
        Schema::table($this->table_name, static function (Blueprint $table) {
            $table->dropIndex([
                'active',
                'slug',
                'position',
                'deleted_at',
            ]);
        });

        Schema::dropIfExists($this->table_name);
    }

}
