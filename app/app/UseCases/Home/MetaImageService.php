<?php
declare(strict_types=1);

namespace App\UseCases\Home;

use App\UseCases\AbstractImageService;

/**
 * Image saver.
 *
 * @package App\UseCases\Home
 */
final class MetaImageService extends AbstractImageService
{
    /**
     * {@inheritDoc}
     */
    public function imageDirectory(): string
    {
        return 'home';
    }

    /**
     * {@inheritDoc}
     */
    protected function generateRelativePath(): string
    {
        return $this->imageDirectory();
    }

}
