<?php
declare(strict_types=1);

namespace App\UseCases;

use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Spatie\ImageOptimizer\OptimizerChain;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

/**
 * Saves and shows images.
 *
 * @package App\UseCases
 */
abstract class AbstractImageService
{
    public const DISK = 'public';

    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * WorkImageService constructor.
     *
     * @param FilesystemFactory $fsFactory
     * @param OptimizerChain $optimizer
     */
    public function __construct(FilesystemFactory $fsFactory, private OptimizerChain $optimizer)
    {
        $this->filesystem = $fsFactory->disk(self::DISK);
    }

    /**
     * Directory name for images relative to storage disk.
     *
     * @return string
     */
    abstract public function imageDirectory(): string;

    /**
     * @return Filesystem
     */
    public function fs(): Filesystem
    {
        return $this->filesystem;
    }

    /**
     * Saves file on disk.
     *
     * @param File|UploadedFile|string $file
     * @param string|null $name
     * @return string|null
     */
    public function store(File|UploadedFile|string $file, ?string $name = null): ?string
    {
        if (\is_string($file)) {
            try {
                $file = new File($file);
            } catch (FileNotFoundException $e) {
                optional(logger())->warning($e->getMessage(), [
                    'service' => static::class,
                    'file' => $file,
                ]);

                return null;
            }
        }

        $file_name = blank($name)
            ? $this->fs()->putFile($this->generateRelativePath(), $file)
            : $this->fs()->putFileAs($this->generateRelativePath(), $file, $name);

        if ($file_name === false) {
            optional(logger())->warning('Image not saved.', [
                'service' => static::class,
                'file' => $file,
            ]);

            return null;
        }

        // optimizes image
        $this->optimizer->optimize(
            $this->fs()->path($file_name)
        );

        return $file_name;
    }

    /**
     * Deletes file from disk.
     *
     * @param string|array $paths
     * @return bool
     */
    public function delete(string|array $paths): bool
    {
        return $this->fs()->delete($paths);
    }

    /**
     * Url to original file.
     *
     * @param string $file
     * @return string
     * @throws \RuntimeException
     */
    public function urlOriginal(string $file): string
    {
        return $this->fs()->url($file);
    }

    /**
     * Url to resized file.
     *
     * @param string $file
     * @param array $params
     * @return string
     * @throws \InvalidArgumentException
     * @see image_glide_url()
     */
    public function urlResized(string $file, array $params = []): string
    {
        return image_glide_url($file, $params);
    }

    /**
     * Path to file.
     *
     * @param string $file
     * @return string
     */
    public function path(string $file): string
    {
        return $this->fs()->path($file);
    }

    /**
     * Relative path to image in public disk.
     *
     * @return string
     */
    protected function generateRelativePath(): string
    {
        return $this->imageDirectory()
            . DIRECTORY_SEPARATOR . strtolower(Str::random(2))
            . DIRECTORY_SEPARATOR . strtolower(Str::random(2));
    }

}
