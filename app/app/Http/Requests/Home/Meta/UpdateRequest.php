<?php
declare(strict_types=1);

namespace App\Http\Requests\Home\Meta;

use SP\Admin\Http\Requests\AbstractFormRequest;

/**
 * Class UpdateRequest.
 *
 * @package App\Http\Requests\Home\Meta
 */
final class UpdateRequest extends AbstractFormRequest
{
    /**
     * Attributes with boolean values.
     *
     * @var array
     */
    protected array $from_checkbox = [
        'del_image',
    ];

    /**
     * Gets the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable|max:195',
            'description' => 'nullable|max:500',
            'image' => 'nullable|image',
            'del_image' => 'boolean',
        ];
    }

}
