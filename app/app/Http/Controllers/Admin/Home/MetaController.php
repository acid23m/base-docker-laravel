<?php
declare(strict_types=1);

namespace App\Http\Controllers\Admin\Home;

use App\Http\Requests\Home\Meta\UpdateRequest;
use App\Models\Home\Meta;
use App\Repositories\Home\MetaRepository;
use App\UseCases\Home\MetaImageService;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use SP\Admin\Http\Controllers\AdminController;

/**
 * CRUD for home meta data.
 *
 * @package App\Http\Controllers\Admin\Home
 */
final class MetaController extends AdminController
{
    /**
     * MetaController constructor.
     *
     * @param MetaRepository $repository
     */
    public function __construct(private MetaRepository $repository)
    {
        parent::__construct();

//        $this->authorizeResource(Meta::class);
    }

    /**
     * {@inheritDoc}
     */
    protected function resourceMethodsWithoutModels(): array
    {
        return ['show', 'edit', 'update'];
    }

    /**
     * Displays the specified resource.
     *
     * @return View
     */
    public function show(): View
    {
        return view('admin.home.metas.show', [
            'modeldetails_config' => $this->repository->modelDetailsConfig(),
        ]);
    }

    /**
     * Shows the form for editing the specified resource.
     *
     * @param MetaImageService $imageService
     * @return View
     */
    public function edit(MetaImageService $imageService): View
    {
        $model = $this->repository->getData();

        return view('admin.home.metas.edit', [
            'model' => $model,
            'image_url' => filled($model->image) ? $imageService->urlOriginal($model->image) : '',
        ]);
    }

    /**
     * Updates the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @return RedirectResponse
     * @throws MassAssignmentException
     */
    public function update(UpdateRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $model = $this->repository->getData();

        if (!$model->fill($data)->save()) {
            return back()->with('error', trans('Error updating.'));
        }

        return redirect()
            ->route('admin.home.metas.show')
            ->with('success', trans('The Record has been updated.'));
    }

}
