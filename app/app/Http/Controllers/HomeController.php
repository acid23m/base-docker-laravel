<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\Home\MetaRepository;
use Illuminate\View\View;

/**
 * Homepage controller.
 *
 * @package App\Http\Controllers
 */
final class HomeController extends Controller
{
    /**
     * Displays homepage.
     *
     * @param MetaRepository $metaRepository
     * @return View
     */
    public function index(MetaRepository $metaRepository): View
    {
        // page meta data
        $metaData = $metaRepository->getFullData();

        $this->seo()->setTitle($metaData['title']);
        $this->seo()->setDescription($metaData['description']);
        $this->seo()->addImages($metaData['image']);

        return view('home.site');
    }

}
