<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Base controller.
 *
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use SEOTools;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $default_title = config('seotools.meta.defaults.title');
        if ($default_title !== false) {
            config([
                'seotools.meta.defaults.title' => config('app.name'),
            ]);
        }
    }

}
