<?php
declare(strict_types=1);

namespace App\Providers;

use App\Models\Home\Meta as HomeMeta;
use App\Observers\Home\MetaObserver as HomeMetaObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws \RuntimeException
     */
    public function boot(): void
    {
        Paginator::useBootstrap();

        // observers
        HomeMeta::observe(HomeMetaObserver::class);
    }

}
