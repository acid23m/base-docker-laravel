<?php
declare(strict_types=1);

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use SP\Admin\Models\Model;
use Illuminate\Support\Carbon;

/**
 * Homepage meta data.
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 *
 * @package App\Models\Home
 */
final class Meta extends Model
{
    use HasFactory;

    public const IMAGE_DIRECTORY = 'home';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'home_metas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'image',
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * {@inheritDoc}
     */
    public static function attributeLabels(): array
    {
        return [
            'title' => trans('Page Title'),
            'description' => trans('Page Description'),
            'image' => trans('Image'),
            'created_at' => trans('Creation Date'),
            'updated_at' => trans('Modification Date'),
        ];
    }

}
