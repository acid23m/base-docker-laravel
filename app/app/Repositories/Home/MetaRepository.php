<?php
declare(strict_types=1);

namespace App\Repositories\Home;

use App\Models\Home\Meta;
use App\UseCases\Home\MetaImageService;
use Illuminate\Contracts\Cache\Factory as Cache;
use SP\Admin\View\Widgets\ModelDetails\Rows\CreatedAtRow;
use SP\Admin\View\Widgets\ModelDetails\Rows\UpdatedAtRow;

/**
 * Retrieves and outputs data.
 *
 * @package App\Repositories\Home
 */
final class MetaRepository
{
    /**
     * MetaRepository constructor.
     *
     * @param MetaImageService $imageService
     * @param Cache $cache
     */
    public function __construct(private MetaImageService $imageService, private Cache $cache)
    {
    }

    /**
     * Retrieves data from database.
     *
     * @return Meta
     */
    public function getData(): Meta
    {
        return Meta::firstOrCreate([
            'id' => 1,
        ], [
            'title' => config('app.name'),
        ]);
    }

    /**
     * Config for modelDetails widget.
     *
     * @return array
     */
    public function modelDetailsConfig(): array
    {
        $model = $this->getData();

        $image_url = transform($model->image, fn($image) => $this->imageService->urlResized($image, [
            'w' => 200,
        ]));

        return [
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'title',
                    'value' => "<strong>{$model->title}</strong>",
                ],
                [
                    'attribute' => 'description',
                    'value' => filled($model->description) ? nl2br($model->description) : '',
                ],
                [
                    'attribute' => 'image',
                    'value' => static function (Meta $model) use (&$image_url): string {
                        if ($image_url) {
                            $str = html()->img($image_url)->class('img-thumbnail')->attributes([
                                'alt' => $model->title,
                            ]);
                            $str .= '<div class="mt-3 text-muted">' . $model->image . '</div>';

                            return $str;
                        }

                        return '-';
                    },
                ],
                CreatedAtRow::class,
                UpdatedAtRow::class,
            ],
        ];
    }

    /**
     * Full data for front.
     *
     * @return array
     */
    public function getFullData(): array
    {
        $context = $this;

        return $this->cache->remember('home_meta', 60, static function () use (&$context) {
            $model = $context->getData();

            return [
                'title' => $model->title,
                'description' => $model->description,
                'image' => filled($model->image) ? $context->imageService->urlOriginal($model->image) : app_logo_url(),
            ];
        });
    }

}
