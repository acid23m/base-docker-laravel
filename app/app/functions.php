<?php declare(strict_types=1);

if (!\function_exists('image_placeholder')) {
    /**
     * Image placeholder.
     *
     * @param array $options [width, height, extension, bg_color, txt_color, text, return_type (path|url)]
     * @return string|null Path to image or NULL on failure
     */
    function image_placeholder(array $options): ?string
    {
        $w = (string)($options['width'] ?? 300);
        $h = (string)($options['height'] ?? 170);
        $size = "{$w}x{$h}";

        $extension = $options['extension'] ?? 'jpg';

        $bg_color = $options['bg_color'] ?? null;
        $txt_color = $options['txt_color'] ?? null;
        $colors = '';
        if ($bg_color !== null && $txt_color !== null) {
            $colors = "/$bg_color/$txt_color";
        }

        $text = isset($options['text']) ? urlencode(e($options['text'])) : urlencode(config('app.name'));

        $return_type = isset($options['return_type']) && in_array($options['return_type'],
            ['path', 'url']) ? $options['return_type'] : 'url';

        $service_url = 'https://via.placeholder.com';
        $url = "$service_url/$size{$colors}.$extension?text=$text";

        if ($return_type === 'url') {
            return $url;
        }

        try {
            $image_source = file_get_contents($url);
            $tmp_image = sys_get_temp_dir() . '/' . md5((string)time());
            file_put_contents($tmp_image, $image_source);
        } catch (\Throwable) {
            $tmp_image = null;
        }

        return $tmp_image;
    }
}
