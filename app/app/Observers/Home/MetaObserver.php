<?php
declare(strict_types=1);

namespace App\Observers\Home;

use App\Models\Home\Meta;
use App\UseCases\Home\MetaImageService;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

/**
 * Events handler.
 *
 * @package App\Observers\Home
 */
final class MetaObserver
{
    /**
     * PostObserver constructor.
     *
     * @param MetaImageService $imageService
     */
    public function __construct(private MetaImageService $imageService)
    {
    }

    /**
     * Handle the model "updating" event.
     *
     * @param Meta $model
     */
    public function updating(Meta $model): void
    {
        /** @var File|UploadedFile|string|bool|null $image */
        $image = $model->image;

        // deletes image
        if ($image === false) {
            $this->imageService->delete(
                $model->getOriginal('image')
            );
            $model->image = null;
        }

        // saves image
        if ($image instanceof File || $image instanceof UploadedFile) {
            $this->imageService->delete(
                $model->getOriginal('image')
            );
            $model->image = $this->imageService->store($image);
        }
    }

}
