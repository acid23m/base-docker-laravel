<?php declare(strict_types=1);

return [
    'static' => [
        [
            'location' => static fn(): string => url(''),
            'priority' => 0.9,
        ],
    ],
    'dynamic' => [],
];
