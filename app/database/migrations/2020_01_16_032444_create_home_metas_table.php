<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Meta data for Homepage.
 */
class CreateHomeMetasTable extends Migration
{
    protected string $table_name = 'home_metas';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create($this->table_name, static function (Blueprint $table) {
            $table->unsignedTinyInteger('id')->default(1);
            $table->string('title')->nullable()->comment('page title');
            $table->text('description')->nullable()->comment('page description');
            $table->string('image')->nullable()->comment('page image');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table($this->table_name, static function (Blueprint $table) {
            $table->dropPrimary(['id']);
        });

        Schema::dropIfExists($this->table_name);
    }

}
