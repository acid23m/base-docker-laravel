<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Home\Meta;
use Illuminate\Database\Seeder;

class HomeMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Meta::factory(1)->create();
    }

}
