<?php declare(strict_types=1);

namespace Database\Factories\Home;

use App\Models\Home\Meta;
use App\UseCases\Home\MetaImageService;
use Illuminate\Database\Eloquent\Factories\Factory;

class MetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Meta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        /** @var MetaImageService $imageService */
        $imageService = resolve(MetaImageService::class);

        $tmp_image = $this->faker->image(null, 800, 600);
        if ($tmp_image === false) {
            $tmp_image = image_placeholder([
                'width' => 800,
                'height' => 600,
                'bg_color' => '666',
                'txt_color' => 'fff',
                'return_type' => 'path',
            ]);
        }

        try {
            $image = $imageService->store($tmp_image);
            \unlink($tmp_image);
        } catch (\Throwable $e) {
            $image = null;
        }

        return [
            'id' => 1,
            'title' => $this->faker->realText(50),
            'description' => $this->faker->realText(120),
            'image' => $image,
        ];
    }
}
