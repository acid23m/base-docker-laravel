<?php declare(strict_types=1);

use App\Http\Controllers\Admin\Home\MetaController;
use App\Http\Controllers\HomeController;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Support\Facades\Route;
use SP\Admin\Http\Middleware\Locale;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::prefix('admin')
    ->name('admin.')
    ->middleware([
        'web',
        Locale::class,
        AuthenticateSession::class,
    ])->group(static function () {
        // home meta
        Route::get('home/metas', [MetaController::class, 'show'])->name('home.metas.show');
        Route::get('home/metas/edit', [MetaController::class, 'edit'])->name('home.metas.edit');
        Route::match(['put', 'patch'], 'home/metas', [MetaController::class, 'update'])->name('home.metas.update');
    });
