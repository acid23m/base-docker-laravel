ARG UBUNTU_VERSION=20.04
FROM ubuntu:${UBUNTU_VERSION}

RUN mkdir -p --mode=777 /tmp /var/cache && \
    apt update && \
    apt upgrade -y && \
    apt dist-upgrade -y && \
    apt install -ym apt-utils tzdata locales && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 && \
    localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8 && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG LANG_VAR=en_US.utf8
#ARG LANG_VAR=ru_RU.utf8
ENV LANG $LANG_VAR

ARG APP_PATH=/app
ENV PATH ${PATH}:$APP_PATH
WORKDIR $APP_PATH

ARG PHP_VERSION=8.0

ARG COMPOSER_AUTH_VAR={}
ENV COMPOSER_AUTH $COMPOSER_AUTH_VAR
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1
ENV COMPOSER_CACHE_DIR /tmp/composer_cache

ARG USER_NAME_VAR=wuser
ENV USER_NAME $USER_NAME_VAR
ARG USER_ID_VAR=1000
ENV USER_ID $USER_ID_VAR

RUN apt update && \
    apt dist-upgrade -y && \
    apt full-upgrade -y && \
    apt install -ym --no-install-recommends --no-install-suggests \
        software-properties-common \
        zip unzip \
        openssl \
        wget \
        curl \
        cron \
        git \
        supervisor && \
    LC_ALL=C.UTF-8 add-apt-repository -y universe && \
    LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt update && \
    apt install -ym --no-install-recommends --no-install-suggests \
        libargon2-1 libidn2-0 libpcre2-8-0 libpcre3 libxml2 libzstd1 \
        "php${PHP_VERSION}" \
        "php${PHP_VERSION}-bcmath" \
        "php${PHP_VERSION}-bz2" \
        "php${PHP_VERSION}-cli" \
        "php${PHP_VERSION}-common" \
        "php${PHP_VERSION}-curl" \
        "php${PHP_VERSION}-fpm" \
        "php${PHP_VERSION}-gd" \
        "php${PHP_VERSION}-intl" \
        "php${PHP_VERSION}-mbstring" \
        "php${PHP_VERSION}-opcache" \
        "php${PHP_VERSION}-sqlite3" \
        "php${PHP_VERSION}-pgsql" \
        "php${PHP_VERSION}-mysql" \
        "php${PHP_VERSION}-xml" \
        "php${PHP_VERSION}-zip" \
        php-imagick \
        php-redis \
        php-uuid \
        php-ds && \
    mkdir -p "/etc/php/${PHP_VERSION}/fpm/php-fpm.d" /run/php && \
    cp -a "/etc/php/${PHP_VERSION}/fpm/php-fpm.conf" "/etc/php/${PHP_VERSION}/fpm/php-fpm.conf.bak" && \
    echo "\ninclude=/etc/php/${PHP_VERSION}/fpm/php-fpm.d/*.conf\n" >> "/etc/php/${PHP_VERSION}/fpm/php-fpm.conf" && \
    ln -sf "/usr/sbin/php-fpm${PHP_VERSION}" /usr/sbin/php-fpm && \
    useradd -M -u "${USER_ID}" -U -G www-data "${USER_NAME}" && \
    chown -R "${USER_ID}:www-data" "${APP_PATH}" && \
    usermod -a -G www-data root && \
    curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    rm composer-setup.php && \
#    mkdir -p /root/.composer /root/.config/composer && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /var/log/php /var/log/fpm /var/log/app && \
    touch /var/log/php/error.log && \
    ln -sf /dev/stderr /var/log/php/error.log

#CMD ["php-fpm"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisor.conf"]

EXPOSE 9000
