#!/bin/sh

# installs additional software
apt update
apt install -ym --no-install-recommends --no-install-suggests pngquant optipng pngcrush jpegoptim gifsicle

# starts cron jobs
echo '* * * * * php artisan schedule:run >> /dev/null 2>&1' > /var/spool/cron/crontabs/root
chmod 600 /var/spool/cron/crontabs/root
chown root:crontab /var/spool/cron/crontabs/root

### --------------------------------------------- ###
supervisord -c /etc/supervisor/conf.d/supervisor.conf
